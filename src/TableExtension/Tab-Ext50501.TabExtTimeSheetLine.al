tableextension 50501 TabExtTimeSheetLine extends "Time Sheet Line"
{
    fields
    {
        field(50500; "Timesheet Ending Date"; Date)
        {
            FieldClass = FlowField;
            CalcFormula = Lookup ("Time Sheet Header"."Ending Date" WHERE ("No." = FIELD ("Time Sheet No.")));
        }
    }

    var
        myInt: Integer;
}