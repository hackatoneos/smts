tableextension 50500 JobRoundingSetup extends Job
{
    fields
    {
        field(50500; "Hours Rounding"; Option)
        {
            DataClassification = ToBeClassified;
            OptionMembers = "0","15","30","60";
            OptionCaption = '0,15,30,60';
        }
    }
}