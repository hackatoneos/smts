table 50501 "Time Sheet Log"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            DataClassification = ToBeClassified;
            AutoIncrement = true;
        }
        field(2; Username; Code[50])
        {
            DataClassification = ToBeClassified;
        }
        field(3; "Job No."; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(4; "TODO No."; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(5; "Activity No."; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(6; "Date"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(7; "From Time"; Time)
        {
            DataClassification = ToBeClassified;
        }
        field(8; "To Time"; Time)
        {
            DataClassification = ToBeClassified;
            trigger OnValidate()
            var
                TimeSheetNo: Code[20];
                TimeSheetLineNo: Integer;
            begin
                if "To Time" = 0T then
                    "To Time" := Time() + 3600000;
                TestField("Job No.");
                TestField("Resource No.");
                if ("TODO No." = '') and ("Job Task Description" = '') then
                    Error('%1 or %2 must be specified.', Rec.FieldCaption("TODO No."), Rec.FieldCaption("Activity No."));
                if (Username <> '') and ("Job No." <> '') and (("TODO No." <> '') OR ("Job Task Description" <> '')) then begin
                    GetTimeSheet(TimeSheetNo, TimeSheetLineNo);
                    Rec.Validate(Time_Sheet_No, TimeSheetNo);
                    Rec.Validate(Time_Sheet_Line_No, TimeSheetLineNo);
                    CreateNewTimeSheetLine(TimeSheetNo, TimeSheetLineNo, Rec);
                    CreateNewTimeSheetDetail(TimeSheetNo, TimeSheetLineNo, Rec);
                    Rec.Validate(Status, Status::close);
                    Rec.Validate(StatusInt, 2);
                    Rec.Modify(TRUE);
                end;

            end;
        }
        field(9; "Time_Sheet_No"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(10; "Time_Sheet_Line_No"; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(11; Status; Option)
        {
            DataClassification = ToBeClassified;
            OptionMembers = "",open,close,sent;
            trigger OnValidate()
            var
                TimeSheetLine: Record "Time Sheet Line";
            begin
                if (Status <> xRec.Status) and
                    (Status = Status::sent) then begin
                    StatusInt := 3;
                    TestField(Time_Sheet_No);
                    TestField(Time_Sheet_Line_No);
                    TimeSheetLine.Get(Time_Sheet_No, Time_Sheet_Line_No);
                    TimeSheetApprovalManagement.Submit(TimeSheetLine);
                end;
            end;
        }
        field(12; "Resource No."; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(13; "Job Task Description"; Text[200])
        {
            DataClassification = ToBeClassified;
        }
        field(14; "From Time Text"; Text[6])
        {
            trigger OnValidate()
            begin
                if "From Time Text" = '0' then
                    Validate("From Time", Time() + 3600000)
                else
                    Evaluate("From Time", "From Time Text");
                //Evaluate("From Time", "From Time Text");
            end;
        }
        field(15; "To Time Text"; Text[6])
        {
            trigger OnValidate()
            begin
                if "To Time Text" = '0' then
                    Validate("To Time", Time() + 3600000)
                else
                    Evaluate("To Time", "To Time Text");
            end;
        }
        field(16; StatusInt; Integer)
        {

        }
    }

    keys
    {
        key("Entry No."; "Entry No.")
        {
            Clustered = true;
        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin
        StatusInt := 1;
    end;

    trigger OnModify()
    begin
        IF Date = 0D then
            Date := WorkDate();
    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

    local procedure GetTimeSheet(VAR TimeSheetNo: Code[20]; VAR TimeSheetLineNo: Integer)
    var
        TimeSheetHeader: Record "Time Sheet Header";
        TimeSheetLine: Record "Time Sheet Line";
    begin
        TimeSheetNo := '';
        TimeSheetLineNo := 0;
        TimeSheetHeader.Reset;
        TimeSheetHeader.SetRange("Resource No.", "Resource No.");
        TimeSheetHeader.SetFilter("Starting Date", '<=%1', Date);
        TimeSheetHeader.SetFilter("Ending Date", '>=%1', Date);
        TimeSheetHeader.FindFirst();
        TimeSheetNo := TimeSheetHeader."No.";
        TimeSheetLine.Reset();
        TimeSheetLine.SetRange("Time Sheet No.", TimeSheetHeader."No.");
        if TimeSheetLine.FindLast() then
            TimeSheetLineNo := TimeSheetLine."Line No." + 10000
        else
            TimeSheetLineNo := 10000;
    end;

    local procedure CreateNewTimeSheetLine(VAR TimeSheetNo: Code[20]; VAR TimeSheetLineNo: Integer; VAR TimeSheetLog: Record "Time Sheet Log")
    var
        TimeSheetLine: Record "Time Sheet Line";
        JobTask: Record "Job Task";
    begin
        TimeSheetLine.Init();
        TimeSheetLine."Time Sheet No." := TimeSheetNo;
        TimeSheetLine."Line No." := TimeSheetLineNo;
        TimeSheetLine.Insert(true);

        TimeSheetLine.Validate("Approver ID", Username);
        if TimeSheetLog."Job Task Description" <> '' then begin
            //JobTask.Get("Job No.", TimeSheetLog."Activity No.");
            //TimeSheetLine.Validate("Job Task No.", JobTask."Job Task No.");
            TimeSheetLine.Validate(Description, "Job Task Description");
        end else
            TimeSheetLine.Validate("Job To-do No.", TimeSheetLog."TODO No.");
        TimeSheetLine.Modify(true);
    end;

    local procedure CreateNewTimeSheetDetail(VAR TimeSheetNo: Code[20]; VAR TimeSheetLineNo: Integer; VAR TimeSheetLog: Record "Time Sheet Log")
    var
        TimeSheetDetail: Record "Time Sheet Detail";
    begin
        TimeSheetDetail.Init();
        TimeSheetDetail."Entry No." := 0;
        TimeSheetDetail.Validate("Time Sheet No.", TimeSheetNo);
        TimeSheetDetail.Validate("Time Sheet Line No.", TimeSheetLineNo);
        TimeSheetDetail.Validate(Date, TimeSheetLog.Date);
        TimeSheetDetail.Insert(true);

        TimeSheetDetail.Validate(Type, TimeSheetDetail.Type::Resource);
        TimeSheetDetail.Validate("Resource No.", TimeSheetLog."Resource No.");
        TimeSheetDetail.Validate("Job No.", TimeSheetLog."Job No.");
        TimeSheetDetail.Validate("From Time", TimeSheetLog."From Time");
        TimeSheetDetail.Validate("To Time", TimeSheetLog."To Time");
        TimeSheetDetail.Modify(true);
    end;

    var
        TimeSheetApprovalManagement: Codeunit "Time Sheet Approval Management";
}