table 50503 "User TODO"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Todo No."; Code[20])
        {
            DataClassification = ToBeClassified;

        }
        field(2; User; code[50])
        {
            trigger OnValidate()
            var
                TimeSheetLog: Record "Time Sheet Log";
                timeSheetLog2: Record "Time Sheet Log";
            begin
                TestField("Todo No.");
                TimeSheetLog.SetRange(Username, User);
                TimeSheetLog.SetRange("TODO No.", "Todo No.");
                TimeSheetLog.SetRange(Status, TimeSheetLog.Status::close);

                if TimeSheetLog.FindSet() then
                    repeat
                        timeSheetLog2.Get(TimeSheetLog."Entry No.");
                        timeSheetLog2.Status := timeSheetLog2.Status::sent;
                        timeSheetLog2.Modify(true);
                    until TimeSheetLog.Next() = 0;

            end;
        }
    }

    keys
    {
        key("Todo No."; "Todo No.")
        {
            Clustered = true;
        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}