table 50500 SMSTSetup
{
    Caption = 'Smart Mobile Time Sheet';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Default Rounding"; Option)
        {
            DataClassification = ToBeClassified;
            OptionMembers = "0","15","30","60";
            OptionCaption = '0,15,30,60';
        }
        field(2; "Primary Key"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
    }

}
