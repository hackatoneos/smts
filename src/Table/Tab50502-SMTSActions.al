table 50502 SMTSActions
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Entry No."; Integer)
        {
            DataClassification = ToBeClassified;
            AutoIncrement = true;
        }
        field(2; "User Name"; Code[50])
        {
            DataClassification = ToBeClassified;
        }
        field(3; "Date"; date)
        {
            DataClassification = ToBeClassified;
        }
        field(4; "Time Sheet No."; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(5; "Time Sheet Line No."; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(20; ActionType; Option)
        {
            OptionMembers = " ","SubmitActivity";
        }

    }

    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    var
        TimeSheetLine: Record "Time Sheet Line";

    begin
        case ActionType of
            ActionType::SubmitActivity:
                begin
                    TestField("User Name");
                    if Date = 0D then
                        Date := WorkDate();
                    TestField("Time Sheet No.");
                    TestField("Time Sheet Line No.");
                    TimeSheetLine.Get("Time Sheet No.", "Time Sheet Line No.");
                    TimeSheetApprovalManagement.Submit(TimeSheetLine);
                end;
        end;
    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

    var
        TimeSheetApprovalManagement: Codeunit "Time Sheet Approval Management";
}