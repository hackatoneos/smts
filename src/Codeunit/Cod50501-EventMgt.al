codeunit 50501 "Event Mgt."
{
    trigger OnRun()
    begin

    end;

    [EventSubscriber(ObjectType::Table, database::"Time Sheet Detail", 'OnAfterQtyFromTimeExecute', '', false, false)]
    local procedure ModifyQuantityOnAfterCopyFromTimeSheetLine(VAR Sender: Record "Time Sheet Detail"; VAR xRec: Record "Time Sheet Detail")
    var
        Job: Record Job;
        SMSTSetup: Record SMSTSetup;

        HoursRounding: Option "0","15","30","60";
        RoudingDecimal: Decimal;
    begin
        Job.Get(Sender."Job No.");
        HoursRounding := job."Hours Rounding";
        if Job."Hours Rounding" = Job."Hours Rounding"::"0" then begin
            SMSTSetup.findfirst();
            HoursRounding := SMSTSetup."Default Rounding";
        end;

        case HoursRounding of
            HoursRounding::"0":
                ;
            HoursRounding::"15":
                CalculateRoundedQty(Sender, 25);
            HoursRounding::"30":
                CalculateRoundedQty(Sender, 50);
            HoursRounding::"60":
                if Sender.Quantity MOD 1 <> 0 then
                    Sender.Quantity := ROUND(sender.Quantity, 1, '>');
        end;

    end;

    local procedure CalculateRoundedQty(var Sender: Record "Time Sheet Detail"; DecimalRounding: Decimal)
    var
        DecimalOriginalQty: Decimal;
        counter: Integer;
    begin
        DecimalOriginalQty := (Sender.Quantity MOD 1) * 100;
        repeat
            DecimalOriginalQty -= DecimalRounding;
            counter += 1;
        until DecimalOriginalQty < 0;
        Sender.Quantity := Sender.Quantity - (Sender.Quantity MOD 1) + counter * DecimalRounding / 100;
    end;
}