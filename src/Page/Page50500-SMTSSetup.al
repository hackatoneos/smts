page 50500 SMTSSetup
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = SMSTSetup;
    InsertAllowed = false;
    DeleteAllowed = false;


    layout
    {
        area(Content)
        {
            group(Setup)
            {
                field("Default Rounding"; "Default Rounding")
                {
                    ApplicationArea = All;
                }
            }
        }
    }



    trigger OnOpenPage()
    begin
        if not Get() then begin
            Init();
            Validate("Default Rounding", "Default Rounding"::"0");
            Insert(true);
        end;
    end;

}