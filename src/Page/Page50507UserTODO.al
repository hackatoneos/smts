page 50507 "User Todo"
{
    PageType = list;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "user Todo";

    layout
    {
        area(Content)
        {
            group(Settings)
            {
                field("Todo No."; "Todo No.")
                {
                    ApplicationArea = All;

                }
                field("User"; User)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin

                end;
            }
        }
    }

    var
        myInt: Integer;
}