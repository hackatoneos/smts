page 50503 "Time Sheet Detail List"
{
    Caption = 'Time Sheet Detail List';
    ApplicationArea = all;
    PageType = List;
    SourceTable = "Time Sheet Detail";
    UsageCategory = Lists;

    layout
    {

        area(content)
        {
            repeater(Group)
            {

                field("Time Sheet No."; "Time Sheet No.")
                {

                }

                field("Time Sheet Line No."; "Time Sheet Line No.")
                {

                }

                field("Date"; "Date")
                {

                }

                field("Type"; "Type")
                {

                }

                field("Resource No."; "Resource No.")
                {

                }

                field("Job No."; "Job No.")
                {

                }

                field("Job Task No."; "Job Task No.")
                {

                }

                field("Cause of Absence Code"; "Cause of Absence Code")
                {

                }

                field("Service Order No."; "Service Order No.")
                {

                }

                field("Service Order Line No."; "Service Order Line No.")
                {

                }

                field("Quantity"; "Quantity")
                {

                }

                field("Posted Quantity"; "Posted Quantity")
                {

                }

                field("Assembly Order No."; "Assembly Order No.")
                {

                }

                field("Assembly Order Line No."; "Assembly Order Line No.")
                {

                }

                field("Status"; "Status")
                {

                }

                field("Posted"; "Posted")
                {

                }

            }
        }
    }

}