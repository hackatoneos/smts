
page 50505 "Time Sheet Log List"
{
    Caption = 'Time Sheet Log List';
    PageType = List;
    SourceTable = "Time Sheet Log";
    UsageCategory = Lists;
    ApplicationArea = all;

    layout
    {

        area(content)
        {
            repeater(Group)
            {

                field("Entry No."; "Entry No.")
                {

                }

                field("Username"; "Username")
                {

                }

                field("Job No."; "Job No.")
                {

                }

                field("TODO No."; "TODO No.")
                {

                }

                field("Activity No."; "Activity No.")
                {

                }

                field("Date"; "Date")
                {

                }

                field("From Time"; "From Time")
                {

                }

                field("To Time"; "To Time")
                {

                }

                field("Time_Sheet_No"; "Time_Sheet_No")
                {

                }

                field("Time_Sheet_Line_No"; "Time_Sheet_Line_No")
                {

                }

                field("Status"; "Status")
                {

                }

                field("Resource No."; "Resource No.")
                {
                }

                field("Job Task Description"; "Job Task Description")
                {

                }
                field("from Time Text"; "From Time Text")
                {

                }
                field("To Time Text"; "To Time Text")
                {

                }
                field(StatusInt; StatusInt)
                {

                }
            }
        }
    }

}