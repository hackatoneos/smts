page 50504 "Time Sheet Comment Line List"
{
    Caption = 'Time Sheet Comment Line List';
    PageType = List;
    SourceTable = "Time Sheet Comment Line";
    UsageCategory = Lists;
    ApplicationArea = All;
    layout
    {

        area(content)
        {
            repeater(Group)
            {

                field("No."; "No.")
                {

                }

                field("Time Sheet Line No."; "Time Sheet Line No.")
                {

                }

                field("Line No."; "Line No.")
                {

                }

                field("Date"; "Date")
                {

                }

                field("Code"; "Code")
                {

                }

                field("Comment"; "Comment")
                {

                }

            }
        }
    }

}