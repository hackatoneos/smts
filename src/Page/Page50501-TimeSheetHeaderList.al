page 50501 "Time Sheet Header List"
{
    PageType = List;
    ApplicationArea = Jobs;
    UsageCategory = Tasks;
    SourceTable = "Time Sheet Header";
    Editable = true;

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field("No."; "No.")
                {
                    ApplicationArea = All;

                }
                field("Starting Date"; "Starting Date")
                {
                    ApplicationArea = All;
                }
                field("Ending Date"; "Ending Date")
                {
                    ApplicationArea = All;
                }
                field("Resource No."; "Resource No.")
                {
                    ApplicationArea = All;
                }
                field("Owner User ID"; "Owner User ID")
                {
                    ApplicationArea = All;
                }
                field("Approver User ID"; "Approver User ID")
                {
                    ApplicationArea = All;
                }
                field("Open Exists"; "Open Exists")
                {
                    ApplicationArea = All;
                }
                field("Submitted Exists"; "Submitted Exists")
                {
                    ApplicationArea = All;
                }
                field("Rejected Exists"; "Rejected Exists")
                {
                    ApplicationArea = All;
                }
                field("Approved Exists"; "Approved Exists")
                {
                    ApplicationArea = All;
                }
                field("Posted Exists"; "Posted Exists")
                {
                    ApplicationArea = All;
                }
                field(Quantity; Quantity)
                {
                    ApplicationArea = All;
                }
                field("Posted Quantity"; "Posted Quantity")
                {
                    ApplicationArea = All;
                }
                field(Comment; Comment)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin

                end;
            }
        }
    }

    var
        myInt: Integer;
}