
page 50506 "SMTSActions List"
{
    Caption = 'SMTSActions List';
    PageType = List;
    SourceTable = "SMTSActions";
    UsageCategory = Lists;
    Editable = false;

    layout
    {

        area(content)
        {
            repeater(Group)
            {

                field("Entry No."; "Entry No.")
                {

                }

                field("User Name"; "User Name")
                {

                }

                field("Date"; "Date")
                {

                }

                field("Time Sheet No."; "Time Sheet No.")
                {

                }

                field("Time Sheet Line No."; "Time Sheet Line No.")
                {

                }

                field("ActionType"; "ActionType")
                {

                }

            }
        }
    }

}