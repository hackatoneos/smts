pageextension 50502 PageExtTimeSheetDetails extends "Time Sheet Details"
{
    layout
    {
        addafter("Job No.")
        {
            field("Time Sheet No."; "Time Sheet No.")
            {
                ApplicationArea = All;
            }
            field("Time Sheet Line No."; "Time Sheet Line No.")
            {
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        // Add changes to page actions here
    }

    var
        myInt: Integer;
}