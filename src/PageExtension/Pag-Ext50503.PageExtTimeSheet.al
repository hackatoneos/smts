pageextension 50503 PageExtTimeSheet extends "Time Sheet"
{
    layout
    {
        addafter("Job To-do No.")
        {
            field("Time Sheet No."; "Time Sheet No.")
            {
                ApplicationArea = All;
            }
            field("Line No."; "Line No.")
            {
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        // Add changes to page actions here
    }

    var
        myInt: Integer;
}